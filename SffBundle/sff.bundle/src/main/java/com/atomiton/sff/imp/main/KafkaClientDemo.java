package com.atomiton.sff.imp.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atomiton.sff.imp.kafka.InputParams;
import com.atomiton.sff.imp.kafka.KafkaClient;

public class KafkaClientDemo
{
   private static final Logger logger = LoggerFactory.getLogger(KafkaClientDemo.class);
   
   public static void main(String[] args) {
	
	   InputParams inputParams = new InputParams();
	   init(inputParams);
	   
	   KafkaClient kafkaClient = new KafkaClient(inputParams, true);
	   
	   kafkaClient.init();
	   kafkaClient.sendMessage();
	   kafkaClient.distroy();
   }
   
   public static void init(InputParams inParams) {
	   inParams.setHostName("54.218.98.6");
	   inParams.setPort("9092");
	   inParams.setKey("CLIENT_ATOMITON");
	   inParams.setMessage("HELLO KAFKA");
	   inParams.setTopicName("CKC_DATA");
   }
}
