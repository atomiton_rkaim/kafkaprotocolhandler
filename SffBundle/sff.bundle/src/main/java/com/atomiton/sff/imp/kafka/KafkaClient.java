package com.atomiton.sff.imp.kafka;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaClient {

	private String topic;
	private String key;
	private String message;
	private String clientId;
	private boolean isAsync;
	private InputParams inputParams;
	private KafkaProducer<String, String> kafkaProducer;
	
	private static final int KAFKA_PRODUCER_BUFFER_SIZE = 64 * 1024;
	private static final int KAFKA_SEND_BUFFER_SIZE = 1024;
	private static final int KAFKA_CONNECTION_TIMEOUT = 100000;
	private static final String KAFKA_ACKS = "all";

	public KafkaClient(InputParams inputParams, boolean isAsync) {
		this.clientId = inputParams.getKey();
		this.inputParams = inputParams;
		this.isAsync = isAsync;
		this.key = inputParams.getKey();
		this.message = inputParams.getMessage();
	}

	public void init() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, inputParams.getHostName() + ":" + inputParams.getPort());
		props.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.ACKS_CONFIG, KafkaClient.KAFKA_ACKS);
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, KafkaClient.KAFKA_PRODUCER_BUFFER_SIZE);
		props.put(ProducerConfig.SEND_BUFFER_CONFIG, KafkaClient.KAFKA_SEND_BUFFER_SIZE);
		props.put(ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG, KafkaClient.KAFKA_CONNECTION_TIMEOUT);
		kafkaProducer = new KafkaProducer<>(props);
		topic = inputParams.getTopicName();
	}

	public void sendMessage() {
		long startTime = System.currentTimeMillis();
		if (isAsync) {
			kafkaProducer.send(new ProducerRecord<>(topic, key, message), new ClientCallBack(startTime, key, message));
		} else {
			try {
				kafkaProducer.send(new ProducerRecord<>(topic, key, message)).get();
				System.out.println("Sent message: (" + key + ", " + message + ")");
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

	public void distroy() {
		if (kafkaProducer != null) {
			kafkaProducer.close();
			System.out.println("Connection Close");
		}
		kafkaProducer = null;
	}

	private class ClientCallBack implements Callback {

		private final long startTime;
		private final String key;
		private final String message;

		public ClientCallBack(long startTime, String key, String message) {
			this.startTime = startTime;
			this.key = key;
			this.message = message;
		}

		public void onCompletion(RecordMetadata metadata, Exception exception) {
			long elapsedTime = System.currentTimeMillis() - startTime;
			if (metadata != null) {
				System.out.println("message(" + key + ", " + message + ") sent to partition(" + metadata.partition()
						+ "), " + "offset(" + metadata.offset() + ") in " + elapsedTime + " ms");
			} else {
				exception.printStackTrace();
			}
		}
	}
}
